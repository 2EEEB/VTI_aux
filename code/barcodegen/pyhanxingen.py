################################################
### example program for VTI class
### simple han xin barcode generator with tk UI
### licensed under WTFPL, feel free to steal
### http://www.wtfpl.net/about/
################################################


from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from tkinter import scrolledtext
import treepoem
import os
import glob
import datetime
from PIL import Image, ImageTk

class Window():
	def __init__(self):
		self.window = Tk()
		self.window.title("Simple Han Xin code generator")
		self.window.geometry("600x440")
		self.window.resizable(0, 0)
		self.default_screen = DefaultScreen(self)
		self.default_screen.render()

	def make_bcd(self):
		try:
			data = str(self.default_screen.ctrl_frame.input_field.get("1.0", END)) #read text from input field
			eclvl = str(self.default_screen.ctrl_frame.lvl.get())
			bcimage = treepoem.generate_barcode(
				barcode_type="hanxin",
				data = data,
				options={"eclevel":eclvl})  #version(size) will be set to smallest possibe without the version option set
			fname = "hxbarcode" + str(datetime.datetime.now().strftime("%y%d%m%H%M%S") + ".png")  #generate filename containing a time stamp
		except Exception as e:
			messagebox.showerror("Error", "Problem creating barcode \n%s" % e)
			return
		
		try:
			bcimage.convert("1").save(fname)  #save image as bw png
			raw_img = Image.open(fname).resize((300,300))  #resize images to fit window, note that large barcodes may become unreadable in the window
			act_img = ImageTk.PhotoImage(raw_img)
		except Exception as e:
			messagebox.showerror("Error", "Problem handling image \n%s" % e)
			return
		
		try:
			if self.default_screen.present_frame.image_label:
				self.default_screen.present_frame.image_label.destroy()  #remove old image from window
			
			self.default_screen.present_frame.image_label = Label(self.default_screen.present_frame, image=act_img)
			self.default_screen.present_frame.image_label.pack(padx=20, pady=70)
			self.default_screen.present_frame.image = act_img  #keep a reference to currently set image so that the garbage collector doesnt eat it
		except Exception as e:
			messagebox.showerror("Error", "Problem displaying image \n%s" % e)
			return

		self.default_screen.ctrl_frame.input_field.delete("1.0", END) #clear input field after generating a barcode

	def clr_entry(self):
		self.default_screen.ctrl_frame.input_field.delete("1.0", END)

	def clean_all(self):
		try:
			self.default_screen.ctrl_frame.input_field.delete("1.0", END)

			if self.default_screen.present_frame.image_label:
				self.default_screen.present_frame.image_label.destroy() #remove old image from window

			files = glob.glob("hxbarcode*.png") #match any files containing the string "hxbarcode"
			for file in files:
				os.remove(file)  #delete any matched files in current directory. move them elsewhere if you need to keep them
		except Exception as e:
			messagebox.showerror("Error", "Problem pruning image files \n%s" % e)
			return

	def run(self):
		self.window.mainloop()

class PresentBcdFrame(Frame):
	def __init__(self, parent):
		super().__init__(parent.window)
		self.image = None
		self.image_label = None

	def render(self):
		self.pack(fill=Y, side=LEFT)

class ControlFrame(Frame):
	def __init__(self, parent):
		super().__init__(parent.window)
		self.parent = parent
		self.lvl = StringVar()
		self.lvl.set("L1")
		self.input_label = Label(self, text="Enter text to encode")
		self.input_field = scrolledtext.ScrolledText(self, width=30, height=15)
		self.clear_txt = ttk.Button(self, text="Clear text", command=self.parent.clr_entry)
		self.clean_all = ttk.Button(self, text="Clean all", command=self.parent.clean_all)
		self.go_button = ttk.Button(self, text="Generate", command=self.parent.make_bcd)
		self.lvl_choice1 = Radiobutton(self, text="EC Level 1", variable=self.lvl, value="L1", command=None)
		self.lvl_choice2 = Radiobutton(self, text="EC Level 2", variable=self.lvl, value="L2", command=None)
		self.lvl_choice3 = Radiobutton(self, text="EC Level 3", variable=self.lvl, value="L3", command=None)
		self.lvl_choice4 = Radiobutton(self, text="EC Level 4", variable=self.lvl, value="L4", command=None)

	def render(self):
		self.pack(fill=Y, side=RIGHT)
		self.input_label.pack(anchor=N)
		self.input_field.pack(anchor=NE, padx=15, pady=5)
		self.clear_txt.pack(anchor=NW, padx=15, pady=1)
		self.lvl_choice1.pack(anchor=E, padx=15)
		self.lvl_choice2.pack(anchor=E, padx=15)
		self.lvl_choice3.pack(anchor=E, padx=15)
		self.lvl_choice4.pack(anchor=E, padx=15)
		self.clean_all.pack(side=LEFT, anchor=SW, padx=10, pady=10)
		self.go_button.pack(side=RIGHT, anchor=SE, padx=10, pady=10)

class DefaultScreen(ControlFrame, PresentBcdFrame):
	def __init__(self, parent):
		self.present_frame = PresentBcdFrame(parent)
		self.ctrl_frame = ControlFrame(parent)

	def render(self):
		self.present_frame.render()
		self.ctrl_frame.render()


def main():

	window = Window()
	window.run()

if __name__ == "__main__":
	main()
