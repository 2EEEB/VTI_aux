# VTI_aux

Collection of code for the VTI class

LaTeX files were compiled using XeLaTeX

###pyhanxingen
A crude Tk GUI Han Xin bar code generator utilizing [python-treepoem](https://github.com/adamchainz/treepoem) wrapper for [BWIPP](https://github.com/bwipp/postscriptbarcode). 

###monoalpha_tool
Implementation of a monoalphabetic substitution "caesar" cipher with an optional Tk gui for encryption and decryption of text


Some parts of the source code may be under a different license.
